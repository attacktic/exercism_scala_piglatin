import models.Word
import utils.Text

object PigLatin {
  val constExcept = List("yt", "xr")
  val ay = "ay"

  def translate(text: String): String = {
    val words: List[String] = text.split(Text.space).map(_.toLowerCase).toList
    words.map { w =>
      if (w.length > 1) {
        rotate(Word(w))
      }
    }.mkString(Text.space)
  }

  private def wordConditionsApply(word: Word, callCount: Int): Boolean = {
    callCount < word.content.length &&
      Text.isConsonant(word.head) &&
      !constExcept.exists(word.content.startsWith) ||
      word.head == 'u' && word.tail == 'q'
  }

  private def rotate(word: Word, callCount: Int = 1): String = {
    if (wordConditionsApply(word, callCount)) {
      rotate(Word(s"${word.body}${word.head}"), callCount + 1)
    } else s"${word.content}$ay"
  }
}