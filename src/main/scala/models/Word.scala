package models

case class Word(
                 content: String
               ) {
  def head: Char = content.charAt(0)

  def tail: Char = content.charAt(content.length - 1)

  def body: String = content.substring(1, content.length)
}