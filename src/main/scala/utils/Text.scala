package utils

object Text {
  val CONSONANTS: String = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"
  val space: String = " "

  def isConsonant(l: Char): Boolean = {
    CONSONANTS.contains(l)
  }
}
